import { env } from "process";

export const DIALECT = "postgres";

const LOCAL_CONFIGURATION = {
    DB: "typeorm-epam",
    PASSWORD: "postgres",
    PORT_DB: 5432,
    SERVER: "localhost",
    USER_DB: "postgres",
};

// TYPEORM_CONNECTION = postgres
// TYPEORM_HOST = localhost
// TYPEORM_PORT = 5432
// TYPEORM_USERNAME = postgres
// TYPEORM_PASSWORD = postgres
// TYPEORM_DATABASE = staging-dump
// TYPEORM_LOGGING = true
// TYPEORM_SYNCHRONIZE = false
// TYPEORM_MIGRATIONS_RUN = true



const PRODUCTION_CONFIGURATION = {
    DB: env.DB,
    PASSWORD: env.PASSWORD,
    PORT_DB: Number(env.PORT_DB),
    SERVER: env.SERVER,
    USER_DB: env.USER_DB,
};

export function isProduction(): boolean {
    return env.NODE_ENV === "PRODUCTION";
}

export const config = {
    //  DATABASE: isProduction() ? PRODUCTION_CONFIGURATION : LOCAL_CONFIGURATION,
    PORT_APP: 8080,
    SECRET: env.SECRET,
};
