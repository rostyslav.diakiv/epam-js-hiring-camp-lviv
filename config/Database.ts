import { createConnection } from "typeorm";
import { env } from "process";
import * as dotenv from "dotenv";
dotenv.config();

export const Connection = createConnection({
  database: env.TYPEORM_DATABASE,
  entities: [env.TYPEORM_ENTITIES],
  host: env.TYPEORM_HOST,
  logging: Boolean(env.TYPEORM_LOGGING),
  password: env.TYPEORM_PASSWORD,
  port: Number(env.TYPEORM_PORT),
  synchronize: Boolean(env.TYPEORM_SYNCHRONIZE),
  type: env.TYPEORM_CONNECTION as any,
  username: env.TYPEORM_USERNAME,
  migrationsRun: Boolean(env.TYPEORM_MIGRATIONS_RUN),
  driver: env.TYPEORM_CONNECTION as any,
  cli: {
    entitiesDir: env.TYPEORM_ENTITIES_DIR,
    migrationsDir: env.TYPEORM_MIGRATIONS_DIR
  }
});
