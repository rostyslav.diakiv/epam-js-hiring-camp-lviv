import * as express from "express";
import { OrderRouter, StatusRouter } from "../app/routes";

interface IROUTER {
    path: string;
    middleware: any[];
    handler: express.Router;
}

const Order = new OrderRouter();
const Status = new StatusRouter();

export const ROUTER: IROUTER[] = [{
    handler: Status.router,
    middleware: [],
    path: "/api/status",
}, {
    handler: Order.router,
    middleware: [],
    path: "/api/orders",
}];
