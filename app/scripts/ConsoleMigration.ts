import * as Fs from "fs";
import * as csv from "csv-parser";
import { Connection } from "../../config/Database";
import { OrderRepository } from "../repository/Order.repository";
import { User } from "../models";
import { UserRepository } from '../repository';

let readResult: any[] = [];

validateArgv();

function onReadEnd() {
  if (process.argv[2] === "users") {
    readResult = readResult.map(({ userId, name, status }) => ({
      id: userId,
      name,
      status
    }));

    Connection.then(connection => {
      connection.getCustomRepository(UserRepository).bulkCreate(readResult);
    });
  } else if (process.argv[2] === "orders") {
    readResult = readResult.map(order => ({
      ...order,
      user: Object.assign(new User(), { id: order.userId })
    }));
    Connection.then(connection => {
      connection.getCustomRepository(OrderRepository).bulkCreate(readResult);
    });
  }
}

Fs.createReadStream(process.argv[3])
  .pipe(csv())
  .on("data", data => readResult.push(data))
  .on("end", onReadEnd)
  .on("error", () => console.log("filePath doesn't exist"));

function validateArgv() {
  if (!process.argv[2]) {
    console.error("type is missing as first arg");
    process.exit();
  }

  // if (process.argv[2] !== 'users' || process.argv[2] !== 'orders') {
  //   console.error('type must be one of [users, orders]');
  //   process.exit()
  // }

  if (!process.argv[3]) {
    console.error("filePath is missing as second arg");
  }
  try {
    Fs.existsSync(process.argv[3]);
  } catch (err) {
    console.log();
    process.exit();
  }
}
