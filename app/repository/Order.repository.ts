import { EntityRepository, Repository } from "typeorm";
import { Order } from "../models";

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
  public findByProductName(
    productName: string,
    offset: number,
    limit: number
  ): Promise<any> {
    return this.createQueryBuilder("order")
      .select("user.id", "id")
      .addSelect("user.name", "name")
      .addSelect("SUM(order.quantity)", "quantity")
      .innerJoin("order.user", "user", "user.status = 'active'")
      .where("order.status = 'purchased'")
      .andWhere("order.product = :productName", { productName })
      .addGroupBy("user.id")
      .orderBy("quantity", "DESC")
      .offset(offset)
      .limit(limit)
      .getRawMany();
  }

  public bulkCreate(orders: Order[]): Promise<any> {
    return this.manager
      .createQueryBuilder()
      .insert()
      .into(Order)
      .values(orders)
      .execute();
  }
}
