import { EntityRepository, Repository } from "typeorm";
import { User } from "../models";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  public bulkCreate(users: User[]): Promise<any> {
    return this.manager
      .createQueryBuilder()
      .insert()
      .into(User)
      .values(users)
      .execute();
  }
}
