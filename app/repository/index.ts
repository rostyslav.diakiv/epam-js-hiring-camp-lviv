import { OrderRepository } from "./Order.repository";
import { UserRepository } from './User.repository';

export { OrderRepository, UserRepository };
