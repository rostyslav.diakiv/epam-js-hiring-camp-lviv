import { Router } from "camesine";
import { StatusController } from "../controllers";

export class StatusRouter extends Router {
    constructor() {
        super(StatusController);
        this.router
            .get("/", this.handler(StatusController.prototype.index));
    }
}
