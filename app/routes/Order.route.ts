import { Router } from "camesine";
import { OrderController } from "../controllers";
import { getOrders } from "../schemas/Order.schema";
import { celebrate } from "celebrate";

export class OrderRouter extends Router {
  constructor() {
    super(OrderController);
    this.router.get(
      "/",
      celebrate(getOrders),
      this.handler(OrderController.prototype.aggregated)
    );
  }
}
