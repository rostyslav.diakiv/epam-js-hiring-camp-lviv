import { Joi } from "celebrate";

export const getOrders = {
  query: {
    product: Joi.string().required(),
    limit: Joi.number(),
    offset: Joi.number()
  }
};
