import { Controller } from "camesine";
import { Request, Response } from "express";
import { getConnection } from "typeorm";

export class StatusController extends Controller {
  constructor(req: Request, res: Response) {
    super(req, res);
  }

  public index(): Response {
    getConnection().isConnected;
    return this.res.send({
      lastUpdated: new Date().toLocaleString(),
      status: getConnection().isConnected ? "good" : "bad"
    });
  }
}
