import { Controller } from "camesine";
import { Request, Response } from "express";
import { OrderService } from "../services";

export class OrderController extends Controller {
  private OrderService: OrderService;

  constructor(req: Request, res: Response) {
    super(req, res);
    this.OrderService = new OrderService();
  }

  public async aggregated(): Promise<Response> {
    const { product, offset, limit } = this.req.query;
    
    const orderList = await this.OrderService.findByProductName(product, offset || 0, limit || 3);
    return this.res.send(orderList);
  }
}
