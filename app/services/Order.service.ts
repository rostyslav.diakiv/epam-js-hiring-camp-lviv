import { getCustomRepository } from "typeorm";
import { OrderRepository } from "../repository/Order.repository";
import { Order } from "../models";

export class OrderService {
  findByProductName(productName: string, offset: number, limit: number): Promise<any> {
      return getCustomRepository(OrderRepository).findByProductName(productName, offset, limit)
  }

  public bulkCreate(orders: Order[]): Promise<Order[]> {
    return getCustomRepository(OrderRepository).bulkCreate(orders);
  }

  public find(): Promise<Order[]> {
    return getCustomRepository(OrderRepository).find();
  }

  public remove(order: Order): Promise<Order> {
    return getCustomRepository(OrderRepository).remove(order);
  }

  public save(order: Order): Promise<Order> {
    return getCustomRepository(OrderRepository).save(order);
  }
}
