import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, ManyToOne, RelationId } from "typeorm";
import { User } from './User.model';

@Entity("order")
export class Order extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public quantity: number;

    @Column()
    public product: string;
    
    @Column()
    public status: string;

    @ManyToOne((type) => User, (user) => user.orders)
    public user: User;
    @RelationId((order: Order) => order.user)
    public readonly userId: number;
}