import { BaseEntity, Column, Entity, PrimaryColumn, OneToMany } from "typeorm";
import { Order } from './Order.model';

@Entity("user")
export class User extends BaseEntity {

    @PrimaryColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public status: string;

    @OneToMany(type => Order, order => order.user)
    orders: Order[];
}